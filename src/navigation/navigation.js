import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Profile from '../pages/Profile';
import HomeScreen from '../pages/Home';
import LandingPageHooks from '../pages/LandingPage';
import  Daftar from '../pages/SignUp';
import Login  from '../pages/Login';
import EditProfile from '../pages/EditProfile'
import ActiveTask from '../pages/ActiveTask';
import CompletedTask from '../pages/CompletedTask';

  const RootStack = createStackNavigator();
  const AuthStack = createStackNavigator();
  const DetailStack = createStackNavigator();
  const Tab = createBottomTabNavigator();
const Navigation = () => {
    return(
        <NavigationContainer independent={true}>
            <RootStack.Navigator initialRouteName="AuthNav" headerMode="none" >
              <RootStack.Screen name="AuthNav" component={authNavigator} />
              <RootStack.Screen name="TabNavigation" component={TabNavigation}/>
              <RootStack.Screen name="Detail" component={Detail}/>
            </RootStack.Navigator>
      </NavigationContainer>
    )
}
export default Navigation;
function authNavigator(){
  return (
      <AuthStack.Navigator initialRouteName="Masuk" headerMode="none" >
        <AuthStack.Screen name="Daftar" component={Daftar} />
        <AuthStack.Screen name="Masuk" component={Login} />
        {/* <AuthStack.Screen name="LandingScreen" component={LandingPageHooks} /> */}
      </AuthStack.Navigator>
  )
}
  function TabNavigation() {
	return (
	  <Tab.Navigator initialRouteName="Home"  >
        <Tab.Screen 
          name="Home"
          component={HomeScreen}
          options={{
            tabBarLabel: 'All Task',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="clipboard-list-outline" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen 
          name="ActiveTask"
          component={ActiveTask}
          options={{
            tabBarLabel: 'Active Task',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="format-list-checkbox" color={color} size={size} />
            ),
          }}
        />  
        <Tab.Screen
          name="CompletedTask"
          component={CompletedTask}
          options={{
          tabBarLabel: 'Completed Task',
          tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="playlist-check" color={color} size={size} />
          ),
        }}
/>
        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
          }}
        />
	  </Tab.Navigator>
	);
  }

  function Detail(){
    return (
        <DetailStack.Navigator  headerMode="none" >
          <DetailStack.Screen name="EditProfile" component={EditProfile} />

        </DetailStack.Navigator>
    )
  }


