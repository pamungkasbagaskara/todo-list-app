import React, { useEffect } from 'react';
// import { View, Text } from 'react-native';
import Daftar from './SignUp';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { CommonActions } from '@react-navigation/native';


import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	Image,
	TextInput,
} from 'react-native';
import { useState } from 'react';
import fetchAxios from '../api/fetchAxios';

const styles = StyleSheet.create({

	bottomView: {

		width: '100%',
		height: 100,
		// backgroundColor: '#FF9800', 
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		bottom: 100
	},
	MainContainer:
	{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		// paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
	},
	textStyle: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		// position: 'absolute',
		marginBottom: 100,
	},
	textStyle2: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
    },
    sub: {

		color: 'white',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	sub2: {

		color: 'blue',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center"
	},
	tinyLogo: {
		width: 75,
		height: 75,
		// marginBottom: 200,
	},
	button: {
		width: "80%",
		backgroundColor: "#008080",
		borderRadius: 25,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10

	},
	loginText: {
		color: "white"
	},
	container: {
		flex: 1,
		backgroundColor: "#c0c0c0",
		alignItems: "center",
		justifyContent: "center",
	},
	TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
	},
	inputView: {
    backgroundColor: "#708090",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
 
    alignItems: "center",
  },

});

function Login({navigation}) {
	const [email, setEmail] = useState('mantap@mail.com');
	const [password, setPassword] = useState('12345678');

	// useEffect(()=> {
	// 	cekToken()
	// },[])

	// const cekToken = async() => {
	// 	try {
	// 		const value = await AsyncStorage.getItem('@token')
	// 		if (value !== null){
	// 			console.log('token=', value)
	// 			navigation.navigate ('TabNavigation',{screen:'Home'});
	// 		}
	// 	}catch(e){
	// 		console.error(e)
	// 	}

	// }
	


	const handleMasuk= async (val) => {
		fetchMasuk(val);
	}
	const fetchMasuk= async (val)=>{
		// const navigation = props;
		
		try{
			const response = await fetchAxios("POST", 'user/login',val)
			console.log('token ==', response.data.token);
			
			if(response.status !== 400){
				AsyncStorage.setItem('@token', response.data.token);
				navigation.navigate ('TabNavigation',{screen:'Home'});
				// navigation.navigate('Daftar')

			}

		}catch(error){
			console.error(error);
		}
	}

	return (
		<View style={styles.container}>
		{/* <Image
				style={styles.tinyLogo}
				source={{
					uri: 'https://reactnative.dev/img/tiny_logo.png',
				}}
		/> */}
		<Text style={styles.textStyle}>Log In</Text>
		{/* <Text style={styles.textStyle}>Email : {this.state.email}</Text> */}
		
		<View style={styles.inputView}>
			<TextInput
				style={styles.TextInput}
				placeholder="Email."
				placeholderTextColor="#003f5c"
				// onChangeText={(textEmail => {this.setState({textEmail})}
				// )}
				onChangeText={(value) => setEmail(value)}
				value={email}
				// onChangeText={(email) => setEmail(email)}
			/>
		</View>
		<View style={styles.inputView}>
			<TextInput
				style={styles.TextInput}
				placeholder="Password."
				placeholderTextColor="#003f5c"
				secureTextEntry={true}
				onChangeText={(value) => setPassword(value)}
				value={password}
				// onChangeText={(password) => setPassword(password)}
			/>
		</View>

		
			<TouchableOpacity 
				style={styles.button} 
				onPress={()=> {
					// alert(`email: ${this.state.email}`)
					handleMasuk ({email, password})
					// this.getData()
					// this.saveData.bind(this)
					
					}}>
						
				<Text style={styles.loginText}>MASUK</Text>
			</TouchableOpacity>
			<Text style={styles.sub}>Belum Punya akun?</Text>
			<Text style={styles.sub2} onPress={() => navigation.navigate('Daftar')} >Daftar</Text>


	</View>
	);
  }


export default Login;

 