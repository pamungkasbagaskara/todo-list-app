import React,{ useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import fetchAxios from '../api/fetchAxios'
import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	Button,
	StatusBar,
	TouchableOpacity,
	ImageBackground,
	Image,
	TextInput,
} from 'react-native';

function EditProfile(props){
    const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [age, setAge] = useState('')
    const avatar = 'https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png'
    const edtProf = async ()=>{
		const {navigation} = props;
		const token = await AsyncStorage.getItem('@token')
		const response = await fetchAxios("PUT", 'user/me',{
            "age": age
        }, {
			'Authorization': `${token}`,
			'Content-Type' : 'application/json'
		  })
		  console.log('editprofile:', response.data.success)
		  if(response.data.success == true){
			
			navigation.navigate('TabNavigation',{screen:'Profile'});
		}}
	

    return(

        <View style={styles.container}>

                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: avatar,
                    }}
                />
            <Text style={styles.textStyle}>EDIT PROFILE</Text>
            {/* </View> */}

            {/* <View style={styles.content}>	 */}
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Name"
                        placeholderTextColor="#003f5c"
                        onChangeText={(value) => setName(value)}
                        // value={this.state.name}
                        // onChangeText={(email) => setEmail(email)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Email"
                        placeholderTextColor="#003f5c"
                        onChangeText={(value) => setEmail(value)}
                        // value={this.state.email}
                        // onChangeText={(password) => setPassword(password)}
                    />
                </View>
                {/* <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Password"
                        placeholderTextColor="#003f5c"
                        secureTextEntry={true}
                        onChangeText={(value) => this.setState({password:value})}
                        value={this.state.password}
                        onChangeText={(password) => setPassword(password)}
                    />
                </View> */}
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Age"
                        placeholderTextColor="#003f5c"
                        onChangeText={(value) => setAge(value)}
                        // value={this.state.age}
                        // onChangeText={(password) => setPassword(password)}
                    />
                </View>
                
                
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.loginText}
                        onPress={() => 
                            edtProf ()
                            // console.log('pressed')
                        }
                        >CONFIRM</Text>
                    </TouchableOpacity>
                    {/* <Text style={styles.sub}>Sudah Punya akun?</Text>
                    <Text style={styles.sub2} onPress={() => navigation.navigate('Masuk')}>Confirm</Text> */}
            {/* </View>		 */}

        </View>
    )

}


export default EditProfile;

const styles = StyleSheet.create({

	textStyle: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		// position: 'absolute',
		marginBottom: 40,
	},
	textStyle2: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
    },
    sub: {

		color: 'white',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	sub2: {

		color: 'blue',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center"
	},
	tinyLogo: {
		marginTop :30,
		width: 150,
		height: 150,
		borderRadius: 200,
	},
	button: {
		width: "80%",
		backgroundColor: "#008080",
		borderRadius: 25,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10

	},
	loginText: {
		color: "white"
	},
	container: {
			flex: 1,
			backgroundColor: "#c0c0c0",
			alignItems: "center",
            justifyContent: "center",
            
	},
	TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    // borderWidth:1,
	},
	inputView: {
    backgroundColor: "#708090",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
     alignItems: "center",
  },
  header:{
      flex:1,
      alignItems: "center",
    //   justifyContent: "flex-start",
      marginTop: 100,

  },
  content:{
	//   flex:2
  }
})
