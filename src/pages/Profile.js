import React,{ useState, useEffect } from 'react';
import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	Button,
	StatusBar,
	TouchableOpacity,
	ImageBackground,
	Image,
	TextInput,
	Modal,
	LogBox
} from 'react-native';
import fetchAxios from '../api/fetchAxios'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Card, Divider } from 'react-native-elements';


function Profile (props){

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [age, setAge] = useState('')
	const [totalTask, setTotalTask] = useState('')
	const [completedTask, setCompletedTask] = useState('')
	const [modalEditProfile, setModalEditProfile] = useState(false)
	const [modalLogOut, setModalLogOut] = useState(false)
	const [modalDelProfile, setModalDelProfile] = useState(false)
	const {navigation} = props;
	const avatar = 'https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png'
	// const [token, setToken]=useState('')
	const token = AsyncStorage.getItem('@token');

	useEffect(()=>{
		// readToken();
		getUser();
		getTask();
		getCompletedTask();
	}, [])

	// const readToken = async () => {
	// 	try {
	// 	  const tokens = await AsyncStorage.getItem('@token')
	// 		setToken(tokens);
	// 		console.log('read token =', tokens)
	// 	} catch (e) {
	// 	  alert('Failed to fetch the data from storage')
	// 	}
	//   }

	const getUser= async ()=>{
		
		try{
			const token = await AsyncStorage.getItem('@token')
			// console.log('get token ==', token);
			const response = await fetchAxios("GET", 'user/me','', {
				'Authorization': `${token}`
			  })
			setName(response.data.name);
			setEmail(response.data.email);
			setAge(response.data.age);
	
		}catch(error){
			console.error(error);
		}
	}
	const getTask= async (val)=>{
		
		try{
			const token = await AsyncStorage.getItem('@token')
			const response = await fetchAxios("GET", 'task','', {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			// console.log('Task Count ==', response.data);
			setTotalTask(response.data.count);
			
		}catch(error){
			console.error(error);
		}
	}
	const getCompletedTask=async (val)=>{
		const token = await AsyncStorage.getItem('@token')
		try{
			const response = await fetchAxios("GET", 'task?completed=true','', {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			// console.log('Task Completed Count ==', response.data);
			setCompletedTask(response.data.count);
			
		}catch(error){
			console.error(error);
		}
	}
	const logOut = async ()=>{
		const {navigation} = props;
		const token = await AsyncStorage.getItem('@token')
		const response = await fetchAxios("POST", 'user/logout','', {
			'Authorization': `${token}`,
			'Content-Type' : 'application/json'
		  })
		  console.log(response.data.success)
		  if(response.data.success == true){
			await AsyncStorage.removeItem('@token');
			navigation.navigate('AuthNav', {screen:'Masuk'});
		}
	}
	const deleteUser = async ()=>{
		const {navigation} = props;
		const token = await AsyncStorage.getItem('@token')
		const response = await fetchAxios("DELETE", 'user/me','', {
			'Authorization': `${token}`,
		  })
		  console.log(response.data)
		setModalDelProfile(false)
		await AsyncStorage.removeItem('@token');
		navigation.navigate('AuthNav', {screen:'Masuk'});
		}
	
	const edtProf = async ()=>{
		const {navigation} = props;
		const token = await AsyncStorage.getItem('@token')
		const response = await fetchAxios("PUT", 'user/me',{
			"name": name,
			"email": email,
			"age": age
        }, {
			'Authorization': `${token}`,
			'Content-Type' : 'application/json'
		  })
		  console.log('editprofile:', response.data.success)
		  getUser()
		  setModalEditProfile(false)
		  
		}


        return (
			<View>
				<View style={styles.container}>
					<Image
						style={styles.tinyLogo}
						source={{
							uri: avatar,
						}}
					/>
					<Text style={{fontSize: 30, fontWeight:'bold', paddingTop:6}}>{name}</Text>
				</View>
				<View style={{flexDirection:'row', alignContent:'center', justifyContent:'center', paddingTop:20}}>
					<View style={{ flex:1, justifyContent:'center', alignSelf:'center', backgroundColor:'#a9a9a9', borderWidth:1}}>
						<Text style={{justifyContent:'center', alignSelf:'center',fontSize:20, fontWeight:'bold', color:'#fff8dc'}}>{totalTask}</Text>
						<Text style={{alignContent:'center', alignSelf:'center', fontSize:15,fontWeight:"500"}} >TOTAL TASK</Text>
					</View>
					<View style={{ flex:1, justifyContent:'center', alignSelf:'center', backgroundColor:'#a9a9a9',borderWidth:1}}>
						<Text style={{justifyContent:'center', alignSelf:'center',fontSize:20, fontWeight:'bold', color:'#fff8dc'}}>{completedTask}</Text>
						<Text style={{alignContent:'center', alignSelf:'center', fontSize:15,fontWeight:"500"}} >TASK COMPLETE</Text>
					</View>
				
				</View>
				<View style={{paddingTop:10, marginLeft:10}}>
					<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Name </Text>
					<Text style={{fontSize:20,}} >{name}</Text>
				</View>
				<View style={{paddingTop:10, marginLeft:10}}>
					<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Email </Text>
					<Text style={{fontSize:20,}} >{email}</Text>
				</View>
				<View style={{paddingTop:10, marginLeft:10}}>
					<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Age </Text>
					<Text style={{fontSize:20,}} >{age}</Text>
				</View>

				<View style={styles.bottomContainer}>
					<TouchableOpacity 
						style={styles.button} 
						onPress={()=> {
							setModalDelProfile (true)
						}}>
													
						<Text style={{color:'white'}}>Delete Profile</Text>
					</TouchableOpacity>
					<TouchableOpacity 
						style={styles.button} 
						onPress={()=> {
							setModalEditProfile(true);							
						}}>
						<Text style={{color:'white'}}>Edit Profile</Text>
					</TouchableOpacity>
					
					<TouchableOpacity 
						style={styles.button}
						onPress={()=> {
							setModalLogOut(true)
						}}>
						<Text style={{color:'white'}}>LogOut</Text>
					</TouchableOpacity>
				</View>





				{/* ----------------MODAL EDIT PROFILE------------------------------------------- */}
			<Modal transparent={true} visible={modalEditProfile}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.6}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>Profile Edit</Text>
						<View style={{paddingTop:10, marginLeft:10}}>
							<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Name </Text>
							<TextInput 
								style={styles.TextInput}
								placeholder= "Edit Name here .."
								defaultValue= {name}
								placeholderTextColor="#003f5c"
								
								onChangeText={(value) => setName(value)}
								// value={desc}
								>
							</TextInput>
						</View>
						<View style={{paddingTop:10, marginLeft:10}}>
							<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Email </Text>
							<TextInput 
								style={styles.TextInput}
								placeholder= "Edit Email here .."
								defaultValue= {email}
								placeholderTextColor="#003f5c"
								
								onChangeText={(value) => setEmail(value)}
								// value={desc}
								>
							</TextInput>
						</View>
						<View style={{paddingTop:10, marginLeft:10}}>
							<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Age </Text>
							<TextInput 
								style={styles.TextInput}
								placeholder= "Edit Age here .."
								defaultValue= {"age"}
								placeholderTextColor="#003f5c"
								
								onChangeText={(value) => setAge(value)}
								// value={desc}
								>
							</TextInput>
						</View>
						<View style={{flexDirection:'row', justifyContent:'flex-end', marginTop: 30}}>
						<TouchableOpacity 
							style={styles.buttonSave} 
							onPress={()=> {
								edtProf()}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>Save</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonCancel} 
							onPress={()=> {							
								setModalEditProfile(false)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>Cancel</Text>
						</TouchableOpacity>		
						</View>			

						
					</View>
					
				</View>
			</Modal>
			
			
			{/* ----------------MODAL LOG OUT------------------------------------------- */}
			<Modal transparent={true} visible={modalLogOut}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.2}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>LOG OUT</Text>
						
						<View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 40}}>
						<TouchableOpacity 
							style={styles.buttonYes} 
							onPress={()=> {
								logOut();
							}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>YES</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonNo} 
							onPress={()=> {							
								setModalLogOut(false)
							}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>NO</Text>
						</TouchableOpacity>		
						</View>				
					</View>
				</View>
			</Modal>
			
			{/* ----------------MODAL DELETE PROFILE------------------------------------------- */}
			<Modal transparent={true} visible={modalDelProfile}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.2}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>DELETE PROFILE</Text>
						
						<View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 40}}>
						<TouchableOpacity 
							style={styles.buttonYes} 
							onPress={()=> {
								deleteUser();
							}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>YES</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonNo} 
							onPress={()=> {							
								setModalDelProfile(false)
							}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>NO</Text>
						</TouchableOpacity>		
						</View>				
					</View>
				</View>
			</Modal>
			
			
			</View>			
        )
}
export default Profile;



const styles = StyleSheet.create({
	buttonYes: {
		width: 80,
		backgroundColor: "#008000",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	buttonNo: {
		width: 80,
		backgroundColor: "#ff0000",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	buttonCancel: {
		width: 80,
		backgroundColor: "#708090",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
		marginLeft:10
	},
	buttonSave: {
		width: 80,
		backgroundColor: "#4169e1",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	tinyLogo: {
		marginTop :30,
		width: 150,
		height: 150,
		borderRadius: 200,

	},
	container: {
		// flex: 1,
		// backgroundColor: "orange",
		alignItems: "center",
		justifyContent: "center",
	},
	bottomContainer: {
		marginLeft: 10,
		marginRight:10,
		paddingTop: 140,
		// flex: 1,
		flexDirection:'row',
		justifyContent: 'space-between',
	},
	button: {
		width: 100,
		backgroundColor: "#c0c0c0",
		borderRadius: 10,
		height: 25,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,

	},
	TextInput: {
		borderBottomWidth : 1,
		},
})