import React,{ useState, useEffect,  } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Card, Divider } from 'react-native-elements';
import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	Button,
	StatusBar,
	TouchableOpacity,
	ImageBackground,
	Image,
	TextInput,
	FlatList,
	TouchableHighlight,
	Modal,
} from 'react-native';
import fetchAxios from '../api/fetchAxios'
import AsyncStorage from '@react-native-async-storage/async-storage';


function CompletedTask(props) {
	const [completedTask, setCompletedTask] = useState([]) ;
	const [modalAdd, setModalAdd] = useState(false);
	const [modalUpdate, setModalUpdate] = useState(false)
	const [modalDelete, setModalDelete] = useState(false)
	const [desc, setDesc] = useState('')
	const [status, setStatus] = useState('')
	const [selectedData, setSelectedData] = useState({})

	const {navigation} = props;
	useEffect(()=>{
		getTask();
		
		// getCompletedTask();
		// getActiveTask();
	}, [])

	const getTask= async (val)=>{
		
		try{
			const token = await AsyncStorage.getItem('@token')
			const response = await fetchAxios("GET", 'task?completed=true','', {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			// console.log('Completed status ==', response.data);
			setCompletedTask(response.data.data);

			
			
		}catch(error){
			console.error(error);
		}
	}

	const addTask= async (val)=>{
		
		try{
			const token = await AsyncStorage.getItem('@token')
			const response = await fetchAxios("POST", 'task',{
				"description": `${desc}`
			}, {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			  console.log(response.data.success)
			  if(response.data.success == true){
				getTask();
				setModalAdd(false)

			}				
			
		}catch(error){
			console.error(error);
		}
	}
	const updateTask= async (id, completed)=>{
		try{
			if (completed == true) {
				updt = false
			} else {
				updt = true
			}
			// console.log('id =', id ,'updt=', updt )
			const token = await AsyncStorage.getItem('@token')
			const response = await fetchAxios("PUT", `task/${id}`,{
				"completed": updt
			}, {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			//   console.log(response.data.success)
			  if(response.data.success == true){
				getTask();
				setModalUpdate(false)
				setSelectedData({});
			}				
			
		}catch(error){
			console.error(error);
		}
	}
	const deleteTask= async (id)=>{
		try{
			
			// console.log('id =', id ,'updt=', updt )
			const token = await AsyncStorage.getItem('@token')
			const response = await fetchAxios("DELETE", `task/${id}`,'', {
				'Authorization': `${token}`,
				'Content-Type' : 'application/json'
			  })
			//   console.log(response.data.success)
			  if(response.data.success == true){
				getTask();
				setModalDelete(false)
				setSelectedData({});
			}				
			
		}catch(error){
			console.error(error);
		}
	}
	renderItem = ({ item }) => {
		return (
			
		  <View style={{ backgroundColor: 'red' }}>
			  <TouchableHighlight 
				style={{ backgroundColor :"#e0ffff" }}
				// onPress={() =>
				// 	// Linking.openURL(url)
				// 	this.tekan ({url:item.url})}
				>
					<Card>
						<Card.Title>{item.description}</Card.Title>
						{/* <Text>{item.completed ? 'Completed' : 'Uncompleted'}</Text> */}
						{item.completed ? (
							<Text>Status : Completed</Text>
							
						): (
							<Text>Status : Uncompleted</Text>
						)}
						{/* <Card.desc>{status}</Card.desc> */}
						<View style={styles.divider}></View>
						<View style={{flexDirection:'row', justifyContent:'flex-end', marginTop: 0}}>
							<TouchableOpacity 
							style={styles.buttonDelete} 
							onPress={()=> {
								// updateTask(item._id, item.completed)
									 setSelectedData(item);
									 setModalDelete(true)
								}}>
							<Text style={{color:'white', alignSelf:'center',}}>Delete Task</Text>
						</TouchableOpacity>
						<TouchableOpacity 
								style={styles.buttonUpdate} 
								onPress={()=> {
									// updateTask(item._id, item.completed)
										setSelectedData(item);
										setModalUpdate(true)
									}}>
								<Text style={{color:'white', alignSelf:'center',}}>Update Data</Text>
							</TouchableOpacity>
							
						</View>
					</Card>
				</TouchableHighlight>
			
			{/* <Text>{item.description}</Text> */}
		  </View>
		);
	  }
	  console.log(selectedData)
	return (
		
		<View style={{flex:1}}>
			
			<View style={ styles.header} >
 				<Text style={styles.headerStyle}>COMPLETED TASK</Text>
			</View>
			
			<FlatList
				data={completedTask}
				renderItem={renderItem}
				keyExtractor={item => item._id}
			/>
			
			<TouchableOpacity 
				style={styles.button} 
				onPress={() =>
					// Linking.openURL(url)
					setModalAdd(true)}>
				<Text style={{color:'blue', fontSize:30, alignSelf:'center',}}>+</Text>
			</TouchableOpacity>
			
			{/* ----------------MODAL ADD TASK------------------------------------------- */}
			<Modal transparent={true} visible={modalAdd}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.4}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>Add New Task</Text>
						<TextInput 
							style={styles.TextInput}
							placeholder="Description...."
							placeholderTextColor="#003f5c"
							onChangeText={(value) => setDesc(value)}
							// value={desc}
							>
						</TextInput>
	
						<View style={{flexDirection:'row', justifyContent:'flex-end', marginTop: 30}}>
						<TouchableOpacity 
							style={styles.buttonSave} 
							onPress={()=> {
								addTask()}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>Save</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonCancel} 
							onPress={()=> {							
								setModalAdd(false)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>Cancel</Text>
						</TouchableOpacity>		
						</View>		
					</View>
				</View>
			</Modal>

			{/* ----------------MODAL UPDATE TASK------------------------------------------- */}		
			<Modal transparent={true} visible={modalUpdate}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.4}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>Update Task</Text>
						<View style={{paddingTop:10, marginLeft:10}}>
							<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Task Description </Text>
							<Text style={{fontSize:20,}} >{selectedData.description}</Text>
						</View>
						<View style={{paddingTop:10, marginLeft:10}}>
							<Text style={{fontSize:12,fontWeight:'500', color:'#696969'}}>Current Status </Text>
							<Text style={{fontSize:20,}} >{selectedData.completed? 'Completed' :'uncomplete' }</Text>
						</View>
						<View style={styles.divider}/>
						<Text style={{fontSize:20,}} >Update task to {selectedData.completed? 'Uncompleted' :'Completed' }?</Text>

						<View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 10}}>
						<TouchableOpacity 
							style={styles.buttonYes} 
							onPress={()=> {
								updateTask(selectedData._id, selectedData.completed)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>YES</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonNo} 
							onPress={()=> {
								
								setModalUpdate(false)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>NO</Text>
						</TouchableOpacity>		
						</View>				
					</View>
				</View>
			</Modal>

			{/* ----------------MODAL DELETE TASK------------------------------------------- */}		
			<Modal transparent={true} visible={modalDelete}>
				<View style={{backgroundColor:'#000000aa', flex:1, justifyContent:'center'}}>
					<View style={{justifyContent:'flex-start', backgroundColor:'#ffffff', margin:20, padding:40, borderRadius:10, flex:0.3}}>
						<Text style={{fontSize:30, alignSelf:'center'}}>DELETE TASK</Text>
						<Text>Description : {selectedData.description}</Text>
						<Text>Status : {selectedData.completed? 'Completed' :'uncomplete' }</Text>
						<Text>Delete Task?</Text>
						<View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 10}}>
						<TouchableOpacity 
							style={styles.buttonYes} 
							onPress={()=> {
								deleteTask(selectedData._id)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>YES</Text>
						</TouchableOpacity>	
						<TouchableOpacity 
							style={styles.buttonNo} 
							onPress={()=> {							
								setModalDelete(false)}}>
							<Text style={{color:'white', alignSelf:'center', justifyContent:'center'}}>NO</Text>
						</TouchableOpacity>		
						</View>				
					</View>
				</View>
			</Modal>
		
		</View>
	)
}


export default CompletedTask;

const styles=StyleSheet.create({
	header:{
 
		width: '100%', 
		height: 30, 
		backgroundColor: '#4169e1', 
		justifyContent: 'center', 
		alignItems: 'center',
		// position: 'absolute',
		top: 0,
	  },
   
	  headerStyle:{
   
		color: 'blue',
		fontSize:22
	  },
	divider : {
		borderBottomColor: 'black',
		borderBottomWidth: 1,
		marginTop: 10,
		marginBottom: 10,
	},
	buttonCancel: {
		width: 80,
		backgroundColor: "#708090",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
		marginLeft:10
	},
	buttonSave: {
		width: 80,
		backgroundColor: "#4169e1",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	buttonYes: {
		width: 80,
		backgroundColor: "#008000",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	buttonNo: {
		width: 80,
		backgroundColor: "#ff0000",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10,
		borderWidth:1,
	},
	buttonUpdate: {
		width: 100,
		backgroundColor: "#cd5c5c",
		borderRadius: 10,
		height: 30,
		alignSelf: "flex-start",
		justifyContent: "center",
		marginLeft: 20,
		// marginBottom: 10,
		borderWidth:1,
	},
	buttonDelete: {
		width: 100,
		backgroundColor: "#8b0000",
		borderRadius: 10,
		height: 30,
		alignSelf: "flex-start",
		justifyContent: "center",
		marginLeft: 20,
		// marginBottom: 10,
		borderWidth:1,
	},
	button: {
		alignSelf: "flex-end",
		borderWidth:1,
		borderColor:'rgba(0,0,0,0.2)',
		justifyContent:'center',
		position:'absolute',
		width:60,
		height:60,
		backgroundColor:'#4169e1',
		borderRadius:50,
		right:10,
		bottom:40,
	},
	buttonSAVE: {
		width: 50,
		backgroundColor: "#c0c0c0",
		borderRadius: 10,
		// height: 25,
		alignSelf: "center",
		// justifyContent: "flex-end",
		marginTop: 30,
		// marginBottom: 10,
		borderWidth:1,
	},
	buttonOK: {
		width: 50,
		backgroundColor: "#c0c0c0",
		borderRadius: 10,
		height: 25,
		alignSelf: "flex-end",
		justifyContent: "flex-end",
		marginTop: 70,
		// marginBottom: 10,
		borderWidth:1,
		
	},
	TextInput: {
		height: 100,
		// flex: 0.7,
		padding: 10,
		marginLeft: 20,
		borderWidth: 1,
		},
})
