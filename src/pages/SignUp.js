// import React from 'react';
// import {
// 	SafeAreaView,
// 	StyleSheet,
// 	ScrollView,
// 	View,
// 	Text,
// 	Button,
// 	StatusBar,
// 	TouchableOpacity,
// 	ImageBackground,
// 	Image,
// 	TextInput,
// } from 'react-native';

// function Daftar() {
    
//         return (

// 			<View>
//             <Text>SignUp PAGE</Text>
// 			</View>
//         )
    
// }


import 'react-native-gesture-handler';
import React from 'react';

import { useState } from 'react';
import fetchAxios from '../api/fetchAxios';

import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	Button,
	StatusBar,
	TouchableOpacity,
	ImageBackground,
	Image,
	TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';



function Daftar({navigation}) {
	const [name, setName] = useState('');	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');	
	const [age, setAge] = useState('');	
	
	const handleDaftar= async (val) => {
		fetchDaftar(val);
	}
	const fetchDaftar= async (val)=>{
		// const navigation = props;
		
		try{
			const response = await fetchAxios("POST", 'user/register',val)
			console.log('token ==', response.data);
			
			if(response.status !== 400){
				AsyncStorage.setItem('@token', response.data.token);
				navigation.navigate ('TabNavigation',{screen:'Home'});
				// navigation.navigate('Daftar')
			
			}

		}catch(error){
			console.error(error);
		}
	}
	return (
		<View style={styles.container}>
			

			 <View style={styles.content}>	
			 <Text style={styles.textStyle}>SIGN UP</Text>
				<View style={styles.inputView}>
					<TextInput
						style={styles.TextInput}
						placeholder="Nama"
						placeholderTextColor="#003f5c"
						onChangeText={(value) => setName(value)}
						value={name}
						// onChangeText={(email) => setEmail(email)}
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
						style={styles.TextInput}
						placeholder="Email"
						placeholderTextColor="#003f5c"
						onChangeText={(value) => setEmail(value)}
						value={email}
						// onChangeText={(password) => setPassword(password)}
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
						style={styles.TextInput}
						placeholder="Password"
						placeholderTextColor="#003f5c"
						secureTextEntry={true}
						onChangeText={(value) => setPassword(value)}
						value={password}
						// onChangeText={(password) => setPassword(password)}
					/>
				</View>
				<View style={styles.inputView}>
					<TextInput
						style={styles.TextInput}
						placeholder="Umur"
						placeholderTextColor="#003f5c"
						onChangeText={(value) => setAge(value)}
						value={age}
						// onChangeText={(password) => setPassword(password)}
					/>
				</View>
				
				
					<TouchableOpacity style={styles.button}>
						<Text style={styles.loginText}onPress={() => handleDaftar ({name, email, password, age})}>DAFTAR</Text>
					</TouchableOpacity>
					<Text style={styles.sub}>Sudah Punya akun?</Text>
					<Text style={styles.sub2} onPress={() => navigation.navigate('Masuk')}>Masuk</Text>
			</View>		

		</View>
		
)};

export default Daftar;



const styles = StyleSheet.create({

	textStyle: {

		color: 'white',
		fontSize: 25,
		// alignItems: 'center',
		// justifyContent: 'center',
		// position: 'absolute',
		marginBottom: 40,
	},
	textStyle2: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
    },
    sub: {

		color: 'white',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	sub2: {

		color: 'blue',
		fontSize: 17,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center"
	},
	tinyLogo: {
		width: 75,
		height: 75,
		// marginBottom: 200,
	},
	button: {
		width: 100,
		backgroundColor: "#008080",
		borderRadius: 25,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10

	},
	loginText: {
		color: "white"
	},
	container: {
			flex: 1,
			backgroundColor: "#c0c0c0",
			
			// alignItems: "center",
            justifyContent: "center",
            
	},
	TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    // borderWidth:1,
	},
	inputView: {
    backgroundColor: "#708090",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
     alignItems: "center",
  },
  header:{
      flex:1,
      alignItems: "center",
    //   justifyContent: "flex-start",
      marginTop: 100,

  },
  content:{
	//   flex:2
	alignItems:'center'
  }

});