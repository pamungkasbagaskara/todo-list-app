import React from 'react';

import {
	SafeAreaView,
	StyleSheet,
	ScrollView,
	View,
	Text,
	Button,
	StatusBar,
	TouchableOpacity,
	ImageBackground,
	Image,
	TextInput,
} from 'react-native';

const styles = StyleSheet.create({

	bottomView: {
		flex:1,
		width: '100%',
		height: 100,
		// backgroundColor: '#FF9800', 
		justifyContent: 'flex-end',
		alignItems: 'center',
		// position: 'absolute',
		bottom: 50
	},
	MainContainer:
	{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-end',
		// backgroundColor:'red',
		// paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
	},
	textStyle: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		// position: 'absolute',
		// marginBottom: 200,
	},
	textStyle2: {

		color: 'white',
		fontSize: 22,
		alignItems: 'center',
		justifyContent: 'center',
		textAlign:'center'
		// position: 'absolute',
		// marginBottom: 500
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center"
	},
	tinyLogo: {
		width: 75,
		height: 75,
		// marginBottom: 200,
	},
	button: {
		width: "80%",
		backgroundColor: "#fb5b5a",
		borderRadius: 25,
		height: 50,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 10

	},
	loginText: {
		color: "white"
	},
	container: {
			flex: 1,
			backgroundColor: "orange",
			alignItems: "center",
			justifyContent: "center",
	},
	TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
	},
	inputView: {
    backgroundColor: "#FFC0CB",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
 
    alignItems: "center",
  },

});
const imagebg = { uri: "https://i.pinimg.com/originals/01/92/43/01924353e48a7af7b7bf3d449b8801b6.jpg" };
const logo = { uri: "https://cdn.freebiesupply.com/logos/large/2x/telkomsel-logo-png-transparent.png" };



// export class LandingPageHooks extends React.Component{
// 	render() {
function LandingPageHooks(props) {
	const {navigation} = props
	return (
		<ImageBackground source={imagebg} style={styles.image}>
		{/* // <View> */}
		<View style={styles.MainContainer}>
			{/* <Text style={styles.textStyle2}> Redu App menjadi sahabat terbaik kamu dalam mencari tempat wisata yang berfaedah dan penuh makna </Text> */}
			<Image
				style={styles.tinyLogo}
				source={{
					uri: 'https://reactnative.dev/img/tiny_logo.png',
				}}
			/>
			<Text style={styles.textStyle}>Aplikasi Untuk Rekreasi dan Edukasi</Text>
			<Text style={styles.textStyle2}> Redu App menjadi sahabat terbaik kamu dalam mencari tempat wisata yang berfaedah dan penuh makna </Text>


		</View>

		<View style={styles.bottomView} >

			<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Masuk')}>
				<Text style={styles.loginText}>MASUK</Text>
			</TouchableOpacity>
			<TouchableOpacity 
			style={styles.button}
			onPress={() => navigation.navigate('Daftar')}
			>
				<Text style={styles.loginText}>DAFTAR</Text>
				
			</TouchableOpacity>

		</View>

		{/* </View> */}
	</ImageBackground> 
	
	);
  }

  export default LandingPageHooks;
